This is a SwiftUI and Core Data application that fetch data about Purchase Order and its Details

Requirements:
1. Xcode 12 or above
2. Minimum deployment:  iOS 14.5

Screenshot:

<img src="image1.png" width="200">
<img src="image2.png" width="200">
<img src="image3.png" width="200">
<img src="image4.png" width="200">
