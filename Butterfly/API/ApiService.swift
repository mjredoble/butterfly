//
//  ApiService.swift
//  PurchaseOrder
//
//  Created by Michael Joseph Redoble on 13/06/2021.
//

import Foundation

final class ApiService {
    var baseURL = ""
    var httpMethod: String?
    var postData: NSMutableDictionary?
    var responseData: Data?
    var timeout = 0
    var responseCode = 0
    var networkRetryCount = 0
    
    func getPurchaseOrders(completionHandler: @escaping ([PurchaseOrderModel]) -> ()) {
        let urlString = "https://my-json-server.typicode.com/butterfly-systems/sample-data/purchase_orders"
        self.httpMethod = "GET"
        
        self.execute(url: urlString, completionHandler: { result in
            if let data = result {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601withFractionalSeconds
                
                do {
                    let orders = try decoder.decode([PurchaseOrderModel].self, from: data)
                    completionHandler(orders)
                }
                catch let error {
                    print("Error: \(error)")
                }
            }
        })
    }
    
    private func execute(url: String, completionHandler: @escaping (Data?) -> ()) {
        let url = URL(string: baseURL + url)!

        var request = URLRequest(url: url)
        request.timeoutInterval = TimeInterval(timeout)
        request.httpMethod = httpMethod
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        if let postData = postData,
           let httpBody = try? JSONSerialization.data(withJSONObject: postData, options: [.prettyPrinted]) {
            request.httpBody = httpBody
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print("HTTP Request Failed \(error)")
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                self.responseCode = httpResponse.statusCode
                print("statusCode: \(httpResponse.statusCode)")
            }
            
            if let data = data {
                completionHandler(data)
            }
        }
        
        task.resume()
    }
}
