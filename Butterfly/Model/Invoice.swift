//
//  Invoice.swift
//  PurchaseOrder
//
//  Created by Michael Joseph Redoble on 13/06/2021.
//

import Foundation

/*
 {
        "id": 11,
        "invoice_number": "101",
        "received_status": 1,
        "created": "2020-05-07T09:32:28.213Z",
        "last_updated_user_entity_id": 10,
        "transient_identifier": "tid",
        "receipts": [
          {
            "id": 110,
            "product_item_id": 110,
            "received_quantity": 20,
            "created": "2020-05-07T09:32:28.213Z",
            "last_updated_user_entity_id": 10,
            "transient_identifier": "tid2",
            "sent_date": "2020-05-07T09:32:28.213Z",
            "active_flag": true,
            "last_updated": "2020-05-07T09:32:28.213Z"
          }
        ],
        "receipt_sent_date": "2020-05-07T09:32:28.213Z",
        "active_flag": true,
        "last_updated": "2020-05-07T09:32:28.213Z"
      }
 */

class Invoice: Codable, Identifiable {
    var id: Int?
    var invoice_number: String?
    var received_status: Int?
    var created: Date?
    var last_updated_user_entity_id: Int?
    var transient_identifier: String?
    var receipts: [Receipt] = []
    var receipt_sent_date: Date?
    var active_flag: Bool?
    var last_updated: Date?
}
