//
//  ItemCore+CoreDataProperties.swift
//  Butterfly
//
//  Created by Michael Joseph Redoble on 15/06/2021.
//
//

import Foundation
import CoreData


extension ItemCore {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ItemCore> {
        return NSFetchRequest<ItemCore>(entityName: "ItemCore")
    }

    @NSManaged public var active_flag: Bool
    @NSManaged public var id: UUID?
    @NSManaged public var item_id: Int16
    @NSManaged public var last_updated: Date?
    @NSManaged public var last_updated_user_entity_id: Int16
    @NSManaged public var product_item_id: Int16
    @NSManaged public var quantity: Int16
    @NSManaged public var transient_identifier: String?
    @NSManaged public var purchaseOrder: PurchaseOrderCore?

}

extension ItemCore : Identifiable {

}
