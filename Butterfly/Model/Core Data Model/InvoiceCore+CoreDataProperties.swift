//
//  InvoiceCore+CoreDataProperties.swift
//  Butterfly
//
//  Created by Michael Joseph Redoble on 15/06/2021.
//
//

import Foundation
import CoreData


extension InvoiceCore {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<InvoiceCore> {
        return NSFetchRequest<InvoiceCore>(entityName: "InvoiceCore")
    }

    @NSManaged public var active_flag: Bool
    @NSManaged public var created: Date?
    @NSManaged public var id: UUID?
    @NSManaged public var invoice_id: Int16
    @NSManaged public var invoice_number: String?
    @NSManaged public var last_updated: Date?
    @NSManaged public var last_updated_user_entity_id: Int16
    @NSManaged public var receipt_sent_date: Date?
    @NSManaged public var received_status: Int16
    @NSManaged public var transient_identifier: String?
    @NSManaged public var receipts: ReceiptCore?
    @NSManaged public var purchaseOrder: PurchaseOrderCore?

}

extension InvoiceCore : Identifiable {

}
