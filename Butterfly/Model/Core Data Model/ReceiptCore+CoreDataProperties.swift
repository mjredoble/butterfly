//
//  ReceiptCore+CoreDataProperties.swift
//  Butterfly
//
//  Created by Michael Joseph Redoble on 15/06/2021.
//
//

import Foundation
import CoreData


extension ReceiptCore {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ReceiptCore> {
        return NSFetchRequest<ReceiptCore>(entityName: "ReceiptCore")
    }

    @NSManaged public var active_flag: Bool
    @NSManaged public var created: Date?
    @NSManaged public var id: UUID?
    @NSManaged public var last_updated: Date?
    @NSManaged public var last_updated_user_entity_id: Int16
    @NSManaged public var product_item_id: Int16
    @NSManaged public var receipt_id: Int16
    @NSManaged public var received_quantity: Int16
    @NSManaged public var sent_date: Date?
    @NSManaged public var transient_identifier: String?
    @NSManaged public var invoice: NSSet?

}

// MARK: Generated accessors for invoice
extension ReceiptCore {

    @objc(addInvoiceObject:)
    @NSManaged public func addToInvoice(_ value: InvoiceCore)

    @objc(removeInvoiceObject:)
    @NSManaged public func removeFromInvoice(_ value: InvoiceCore)

    @objc(addInvoice:)
    @NSManaged public func addToInvoice(_ values: NSSet)

    @objc(removeInvoice:)
    @NSManaged public func removeFromInvoice(_ values: NSSet)

}

extension ReceiptCore : Identifiable {

}
