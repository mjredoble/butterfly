//
//  PurchaseOrderCore+CoreDataProperties.swift
//  Butterfly
//
//  Created by Michael Joseph Redoble on 15/06/2021.
//
//

import Foundation
import CoreData


extension PurchaseOrderCore {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PurchaseOrderCore> {
        return NSFetchRequest<PurchaseOrderCore>(entityName: "PurchaseOrderCore")
    }

    @NSManaged public var active_flag: Bool
    @NSManaged public var approval_status: Int16
    @NSManaged public var delivery_note: String?
    @NSManaged public var device_key: String?
    @NSManaged public var id: UUID?
    @NSManaged public var issue_date: Date?
    @NSManaged public var last_updated: Date?
    @NSManaged public var last_updated_user_entity_id: Int16
    @NSManaged public var order_id: Int16
    @NSManaged public var preferred_delivery_date: Date?
    @NSManaged public var purchase_order_number: String?
    @NSManaged public var sent_date: Date?
    @NSManaged public var server_timestamp: Int16
    @NSManaged public var status: Int16
    @NSManaged public var stock_purchase_process_ids: [Int]?
    @NSManaged public var supplier_id: Int16
    @NSManaged public var items: NSSet?
    @NSManaged public var invoices: NSSet?

}

// MARK: Generated accessors for items
extension PurchaseOrderCore {

    @objc(addItemsObject:)
    @NSManaged public func addToItems(_ value: ItemCore)

    @objc(removeItemsObject:)
    @NSManaged public func removeFromItems(_ value: ItemCore)

    @objc(addItems:)
    @NSManaged public func addToItems(_ values: NSSet)

    @objc(removeItems:)
    @NSManaged public func removeFromItems(_ values: NSSet)

}

// MARK: Generated accessors for invoices
extension PurchaseOrderCore {

    @objc(addInvoicesObject:)
    @NSManaged public func addToInvoices(_ value: InvoiceCore)

    @objc(removeInvoicesObject:)
    @NSManaged public func removeFromInvoices(_ value: InvoiceCore)

    @objc(addInvoices:)
    @NSManaged public func addToInvoices(_ values: NSSet)

    @objc(removeInvoices:)
    @NSManaged public func removeFromInvoices(_ values: NSSet)

}

extension PurchaseOrderCore : Identifiable {

}
