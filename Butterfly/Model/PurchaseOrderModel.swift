//
//  PurchaseOrder.swift
//  PurchaseOrder
//
//  Created by Michael Joseph Redoble on 13/06/2021.
//

import Foundation

class PurchaseOrderModel: Codable, Identifiable {
    var id: Int?
    var supplier_id: Int?
    var purchase_order_number: String?
    var stock_purchase_process_ids: [Int] = []
    var issue_date: Date?
    var items: [Item] = []
    var invoices: [Invoice] = []
    var cancellations: [Cancellation] = []
    var status: Int?
    var active_flag: Bool?
    var last_updated: Date?
    var last_updated_user_entity_id: Int?
    var sent_date: Date?
    var server_timestamp: Int?
    var device_key: String?
    var approval_status: Int?
    var preferred_delivery_date: Date?
    var delivery_note: String?
    
    
    static func getSampleData() -> [PurchaseOrderModel] {
        let orders = Bundle.main.decode([PurchaseOrderModel].self, from: "po.json")
        return orders
    }
    
    static func getSampleItem() -> PurchaseOrderModel {
        let orders = Bundle.main.decode([PurchaseOrderModel].self, from: "po.json")
        return orders[0]
    }
}
