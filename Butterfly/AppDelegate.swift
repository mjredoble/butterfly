//
//  AppDelegate.swift
//  Butterfly
//
//  Created by Michael Joseph Redoble on 14/06/2021.
//

import UIKit
import CoreData

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        fetchAndSaveData()
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Butterfly")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    //MARK: - Call API and save to Core Data
    func fetchAndSaveData() {
        ApiService().getPurchaseOrders(completionHandler: { [self] results in
            for order in results {
                do {
                    //If new data from server delete existing entry and add the latest entry
                    self.updateNewData(id: order.id!, lastUpdatedDate: order.last_updated!, entry: order)
                    
                    if !self.isExist(id: order.id!) {
                        let purchaseOrder = PurchaseOrderCore(context: self.persistentContainer.viewContext)
                        purchaseOrder.id = UUID()
                        purchaseOrder.order_id =  Int16(order.id!)
                        purchaseOrder.supplier_id = Int16(order.supplier_id!)
                        purchaseOrder.stock_purchase_process_ids = order.stock_purchase_process_ids
                        purchaseOrder.purchase_order_number = order.purchase_order_number!
                        purchaseOrder.issue_date = order.issue_date!
                        purchaseOrder.status = Int16(order.status!)
                        purchaseOrder.active_flag = order.active_flag!
                        purchaseOrder.last_updated = order.last_updated!
                        purchaseOrder.last_updated_user_entity_id = Int16(order.last_updated_user_entity_id!)
                        purchaseOrder.sent_date = order.sent_date!
                        purchaseOrder.server_timestamp = Int16(order.server_timestamp!)
                        purchaseOrder.device_key = order.device_key!
                        purchaseOrder.approval_status = Int16(order.approval_status!)
                        purchaseOrder.preferred_delivery_date = order.preferred_delivery_date!
                        purchaseOrder.delivery_note = order.delivery_note!
                        
                        for item in order.items {
                            let itemCore = ItemCore(context: self.persistentContainer.viewContext)
                            itemCore.id = UUID()
                            itemCore.item_id = Int16(item.id!)
                            itemCore.product_item_id = Int16(item.product_item_id!)
                            itemCore.quantity = Int16(item.quantity!)
                            itemCore.last_updated_user_entity_id = Int16(item.last_updated_user_entity_id!)
                            itemCore.transient_identifier = item.transient_identifier!
                            itemCore.active_flag = item.active_flag!
                            itemCore.last_updated = item.last_updated!
                            
                            purchaseOrder.addToItems(itemCore)
                        }
                        
                        for invoice in order.invoices {
                            let inv = InvoiceCore(context: self.persistentContainer.viewContext)
                            inv.id = UUID()
                            inv.invoice_id = Int16(invoice.id!)
                            inv.invoice_number = invoice.invoice_number!
                            inv.received_status = Int16(invoice.received_status!)
                            inv.created = invoice.created!
                            inv.last_updated_user_entity_id = Int16(invoice.last_updated_user_entity_id!)
                            inv.transient_identifier = invoice.transient_identifier!
                            inv.receipt_sent_date = invoice.receipt_sent_date!
                            inv.active_flag = invoice.active_flag!
                            inv.last_updated = invoice.last_updated!
                            
                            for rec in invoice.receipts {
                                let receipt = ReceiptCore(context: self.persistentContainer.viewContext)
                                receipt.id = UUID()
                                receipt.receipt_id = Int16(rec.id!)
                                receipt.product_item_id = Int16(rec.product_item_id!)
                                receipt.received_quantity = Int16(rec.received_quantity!)
                                receipt.created = rec.created!
                                receipt.last_updated_user_entity_id = Int16(rec.last_updated_user_entity_id!)
                                receipt.transient_identifier = rec.transient_identifier!
                                receipt.sent_date = rec.sent_date!
                                receipt.active_flag = rec.active_flag!
                                receipt.last_updated = rec.last_updated!
                                
                                receipt.addToInvoice(inv)
                            }
                            
                            purchaseOrder.addToInvoices(inv)
                        }
                        
                        try self.persistentContainer.viewContext.save()
                    }
                }
                catch let error {
                    print("Error: \(error)")
                }
            }
        })
    }
    
    func isExist(id: Int) -> Bool {
        let fetchRequest = PurchaseOrderCore.fetchRequest() as NSFetchRequest<PurchaseOrderCore>
        let idPred = NSPredicate(format: "order_id = %d", id)
        let predicates = NSCompoundPredicate(type: .or, subpredicates: [idPred])
        fetchRequest.predicate = predicates
        
        let res = try! self.persistentContainer.viewContext.fetch(fetchRequest)
        return res.count > 0 ? true : false
    }
    
    func updateNewData(id: Int, lastUpdatedDate: Date, entry: PurchaseOrderModel) {
        let fetchRequest = PurchaseOrderCore.fetchRequest() as NSFetchRequest<PurchaseOrderCore>
        let pred = NSPredicate(format: "order_id = %d AND last_updated < %@", id, lastUpdatedDate as NSDate)
        let predicates = NSCompoundPredicate(type: .or, subpredicates: [pred])
        fetchRequest.predicate = predicates
        
        if let res = try? self.persistentContainer.viewContext.fetch(fetchRequest) {
            if res.count > 0 {
                let order = res.first! as PurchaseOrderCore
                persistentContainer.viewContext.delete(order)
            }
        }
    }
}

