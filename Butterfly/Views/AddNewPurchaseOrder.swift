//
//  AddNewPurchaseOrder.swift
//  Butterfly
//
//  Created by Michael Joseph Redoble on 15/06/2021.
//

import SwiftUI

struct AddNewPurchaseOrder: View {
    
    @Environment(\.managedObjectContext) var moc
    @Environment(\.presentationMode) var presentationMode
    
    @State var orderId: Int
    
    @State private var supplierId = "11"
    @State private var purchaseOrderNumber = "200"
    @State private var issueDate = Date()
    @State private var status = false
    @State private var isActiveFlag = false
    @State private var lastUpdated = Date()
    @State private var lastUpdatedUserEntityId = 0
    @State private var sentDate = Date()
    @State private var serverTimestamp = 0
    @State private var deviceKey = "string"
    @State private var approvalStatus = false
    @State private var preferredDeliveryDate = Date()
    @State private var deliveryNote = ""
    
    var body: some View {
        Form {
            Section(header: Text("")) {
                Text("ID: \(orderId)")
                
                HStack {
                    Text("Supplier ID: ")
                    TextField("Supplier ID", text: $supplierId)
                }

                HStack {
                    Text("Purchase Order No: ")
                    TextField("Purchase Order Number", text: $purchaseOrderNumber)
                }
                
                Toggle("Status: ", isOn: $status.animation())
                
                Toggle("Active: ", isOn: $isActiveFlag.animation())
                
                Toggle("Approval Status: ", isOn: $approvalStatus.animation())
                
                HStack {
                    Text("Note: ")
                    TextField("Delivery Note", text: $deliveryNote)
                }
            }
            
            Section {
                HStack(alignment: .center, spacing: 10, content: {
                    Spacer()
                    
                    Button("Save") {
                        let newPO = PurchaseOrderCore(context: self.moc)
                        newPO.id = UUID()
                        newPO.order_id = Int16(orderId)
                        newPO.supplier_id = Int16(supplierId)!
                        newPO.purchase_order_number = purchaseOrderNumber
                        newPO.issue_date = issueDate
                        newPO.status = Int16(truncating: NSNumber(value:status))
                        newPO.active_flag = (Int16(truncating: NSNumber(value:isActiveFlag)) != 0)
                        newPO.last_updated = lastUpdated
                        newPO.last_updated_user_entity_id = Int16(lastUpdatedUserEntityId)
                        newPO.sent_date = sentDate
                        newPO.server_timestamp = Int16(serverTimestamp)
                        newPO.device_key = deviceKey
                        newPO.approval_status = Int16(truncating: NSNumber(value:approvalStatus))
                        newPO.preferred_delivery_date = Date()
                        newPO.delivery_note = deliveryNote
                        
                        try? self.moc.save()
                        
                        self.presentationMode.wrappedValue.dismiss()
                    }
                    
                    Spacer()
                })
            }
        }
        .navigationTitle("New Item")
        .navigationBarTitleDisplayMode(.inline)
    }
}
