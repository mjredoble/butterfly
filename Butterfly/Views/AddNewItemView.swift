//
//  AddNewItemView.swift
//  Butterfly
//
//  Created by Michael Joseph Redoble on 15/06/2021.
//

import SwiftUI

struct AddNewItemView: View {
    
    @Environment(\.managedObjectContext) var moc
    @Environment(\.presentationMode) var presentationMode
    
    @State var order: PurchaseOrderCore
    @State private var itemId = 1
    @State private var productItemId = 1
    @State private var transientIdentifier = "tid"
    @State private var lastUpdatedUserEntityId = 1
    @State private var isActiveFlag = false
    @State private var showingAlert = false
    @State private var quantity = ""
    
    var body: some View {
        Form {
            Section(header: Text("")) {
                Text("ID: \(itemId)")
                Text("Product Item ID: \(productItemId)")
                Text("Transient ID: \(transientIdentifier)")
                Toggle("Is it an active item?", isOn: $isActiveFlag.animation())
            }
            
            Section(header: Text("Quantity")) {
                TextField("Quantity", text: $quantity)
                    .keyboardType(.numberPad)
            }
            
            Section {
                HStack(alignment: .center, spacing: 10, content: {
                    Spacer()
                    
                    Button("Add Item") {
                        let newItem = ItemCore(context: self.moc)
                        newItem.id = UUID()
                        newItem.item_id = Int16(itemId)
                        newItem.product_item_id = Int16(productItemId)
                        newItem.quantity = Int16(quantity) ?? 0
                        newItem.last_updated_user_entity_id = Int16(lastUpdatedUserEntityId)
                        newItem.active_flag = (Int16(truncating: NSNumber(value:isActiveFlag)) != 0)
                        newItem.last_updated = Date()
                        
                        self.order.addToItems(newItem)
                        try? self.moc.save()
                        
                        self.presentationMode.wrappedValue.dismiss()
                    }
                    
                    Spacer()
                })
            }
        }
        .navigationTitle("New Item")
        .navigationBarTitleDisplayMode(.inline)
        .onAppear() {
            let items = order.items?.allObjects as! [ItemCore]
            let sortedItems = items.sorted(by: { $0.item_id < $1.item_id })
            if let lastItem = sortedItems.last {
                self.itemId = Int(lastItem.item_id) + 1
                self.productItemId = Int(lastItem.product_item_id) + 1
                self.lastUpdatedUserEntityId = Int(lastItem.last_updated_user_entity_id)
            }
        }
    }
}
