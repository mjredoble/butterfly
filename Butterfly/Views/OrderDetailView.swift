//
//  OrderDetailView.swift
//  Butterfly
//
//  Created by Michael Joseph Redoble on 15/06/2021.
//

import SwiftUI

struct OrderDetailView: View {
    @Environment(\.managedObjectContext) var moc
    @State private var showingAddItemScreen = false
    
    var fetchRequest: FetchRequest<PurchaseOrderCore>
    
    var orderId = 1
    
    let colors: [Int: Color] = [1: .green, 2: .red]
    
    var body: some View {
        let order = fetchRequest.wrappedValue.first! as PurchaseOrderCore
        let items = order.items?.allObjects as! [ItemCore]
        let sortedItems = items.sorted(by: { $0.item_id < $1.item_id })
        
        List {
            Section(header: Text("Items")) {
                ForEach(sortedItems, id: \.self) { item in
                    let orderItem = item
                    
                    HStack {
                        VStack(alignment: .leading, spacing: 10, content: {
                            Text("ID: \(orderItem.item_id)")
                            Text("Quantity: \(orderItem.quantity)")
                        })
                        
                        Spacer()
                        
                        Text("\(order.last_updated?.getShortDate() ?? Date().getShortDate())")
                    }
                }
                
                Button(action: {
                    self.showingAddItemScreen.toggle()
                }) {
                    Text("Add New Item")
                }
                .sheet(isPresented: $showingAddItemScreen) {
                    AddNewItemView(order: order).environment(\.managedObjectContext, self.moc)
                }
            }
            
            Section(header: Text("Invoices")) {
                ForEach(Array(order.invoices! as Set), id: \.self) { item in
                    let invoice = item as! InvoiceCore
                    
                    HStack {
                        VStack(alignment: .leading, spacing: 10, content: {
                            Text("No: \(invoice.invoice_number!)")
                            Text("\(invoice.received_status)")
                                .font(.caption)
                                .fontWeight(.black)
                                .padding(5)
                                .background(colors[Int(invoice.received_status), default: .black])
                                .foregroundColor(Color.white)
                                .clipShape(Circle())
                        })
                        
                        Spacer()
                        
                        Text("\(order.last_updated?.getShortDate() ?? Date().getShortDate())")
                    }
                }
            }
        }
        .navigationTitle("ID: \(orderId)")
        .listStyle(GroupedListStyle())
    }
    
    init(orderId: Int) {
        self.orderId = orderId
        
        let predicate = NSPredicate(format: "order_id == %d", orderId)
        fetchRequest = FetchRequest<PurchaseOrderCore>(entity: PurchaseOrderCore.entity(), sortDescriptors: [], predicate: predicate)
    }
}

struct OrderDetailView_Previews: PreviewProvider {
    static var previews: some View {
        OrderDetailView(orderId: 1)
    }
}
