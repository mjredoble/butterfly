//
//  PurchaseOrderRow.swift
//  Butterfly
//
//  Created by Michael Joseph Redoble on 14/06/2021.
//

import SwiftUI

struct PurchaseOrderRow: View {
    var order: PurchaseOrderCore
    
    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 10, content: {
                Text("ID: \(order.order_id)")
                Text("Items: \(order.items!.count)")
            })
            
            Spacer()
            Text("\(order.last_updated?.getShortDate() ?? Date().getShortDate())")
        }
    }
}

struct PurchaseOrderRow_Previews: PreviewProvider {
    static var previews: some View {
        PurchaseOrderRow(order: PurchaseOrderCore())
    }
}
