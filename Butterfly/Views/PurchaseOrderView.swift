//
//  ContentView.swift
//  Butterfly
//
//  Created by Michael Joseph Redoble on 14/06/2021.
//

import SwiftUI

struct PurchaseOrderView: View {
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(entity: PurchaseOrderCore.entity(), sortDescriptors: []) var orders: FetchedResults<PurchaseOrderCore>
    @State private var showingAddPOScreen = false
    
    var body: some View {
        NavigationView {
            List {
                ForEach(orders, id: \.id) { item in
                    NavigationLink(destination: OrderDetailView(orderId: Int(item.order_id))) {
                        PurchaseOrderRow(order: item)
                    }
                    
                }
            }
            .navigationTitle("Purchase Orders")
            .navigationBarItems(trailing:
                Button(action: {
                    self.showingAddPOScreen.toggle()
                }) {
                    Image(systemName: "plus.circle.fill").imageScale(.large)
                }
                .sheet(isPresented: $showingAddPOScreen) {
                    let newID = orders.count + 1
                    AddNewPurchaseOrder(orderId: newID).environment(\.managedObjectContext, self.moc)
                }
            )
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        PurchaseOrderView()
    }
}
