//
//  PurchaseOrderDetailView.swift
//  Butterfly
//
//  Created by Michael Joseph Redoble on 14/06/2021.
//

import SwiftUI

struct PurchaseOrderDetailView: View {
    @Environment(\.managedObjectContext) var moc
    @State var order: PurchaseOrderCore
    
    @State private var showingAddItemScreen = false
    
    @State private var listOfItems: [ItemCore] = []
    
    let colors: [Int: Color] = [1: .green, 2: .red]
    
    var body: some View {
        List {
            Section(header: Text("Items")) {
                ForEach(listOfItems, id: \.self) { item in
                    let orderItem = item 
                    
                    HStack {
                        VStack(alignment: .leading, spacing: 10, content: {
                            Text("ID: \(orderItem.item_id)")
                            Text("Quantity: \(orderItem.quantity)")
                        })
                        
                        Spacer()
                        
                        Text("\(order.last_updated?.getShortDate() ?? Date().getShortDate())")
                    }
                }
                
                Button(action: {
                    self.showingAddItemScreen.toggle()
                }) {
                    Text("Add New Item")
                }
                .sheet(isPresented: $showingAddItemScreen) {
                    AddNewItemView(order: order).environment(\.managedObjectContext, self.moc)
                }
            }
            
            Section(header: Text("Invoices")) {
                ForEach(Array(order.invoices! as Set), id: \.self) { item in
                    let invoice = item as! InvoiceCore
                    
                    HStack {
                        VStack(alignment: .leading, spacing: 10, content: {
                            Text("No: \(invoice.invoice_number!)")
                            Text("\(invoice.received_status)")
                                .font(.caption)
                                .fontWeight(.black)
                                .padding(5)
                                .background(colors[Int(invoice.received_status), default: .black])
                                .foregroundColor(Color.white)
                                .clipShape(Circle())
                        })
                        
                        Spacer()
                        
                        Text("\(order.last_updated?.getShortDate() ?? Date().getShortDate())")
                    }
                }
            }
        }
        .navigationTitle("ID: \(order.order_id)")
        .listStyle(GroupedListStyle())
        .onAppear() {
            let items = order.items?.allObjects as! [ItemCore]
            let sortedItems = items.sorted(by: { $0.item_id < $1.item_id })
            self.listOfItems = sortedItems
        }
    }
}

struct PurchaseOrderDetailView_Previews: PreviewProvider {
    static var previews: some View {
        PurchaseOrderDetailView(order: PurchaseOrderCore())
    }
}
